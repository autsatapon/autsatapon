﻿# encoding:utf-8

Given /^I am on google\.com$/ do 
  visit('/')
end

When /^I enter "([^"]*)"$/ do |term|
  fill_in('keyword',:with => term)
  page.should have_xpath('//a/img[@id="go"]')
  find(:xpath, '//a/img[@id="go"]').click
end

Then /^I should see results$/ do
  page.should have_content('ไม่พบสินค้า')
end

